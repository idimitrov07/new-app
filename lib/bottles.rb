class Bottles
  def verse(num_beers)
    beers = <<-VERSE
#{first_sentence(num_beers)}
#{second_sentence(num_beers)}
VERSE
  end

  def song
    song = ""
    99.downto(0) do |i|
       i  > 0 ? song += "#{verse(i)}\n" :  song += "#{verse(i)}"
    end
    song
  end

  def verses(num_beers, num_beers_2)
    song = "#{verse(num_beers)}"
    if num_beers_2 == 0
      song += "\n#{verse(1)}"
    end
    song += "\n#{verse(num_beers_2)}"
  end

  def first_sentence(num_beers)
    if num_beers == 0
      "No more bottles of beer on the wall, no more bottles of beer."
    elsif num_beers == 1
      "1 bottle of beer on the wall, 1 bottle of beer."
    else
      "#{num_beers} bottles of beer on the wall, #{num_beers} bottles of beer."
    end
  end

  def second_sentence(num_beers)
    if num_beers == 0
      "Go to the store and buy some more, 99 bottles of beer on the wall."
    elsif num_beers == 1
      "Take it down and pass it around, no more bottles of beer on the wall."
    else
      "Take one down and pass it around, #{num_beers - 1} #{format_string(num_beers)} of beer on the wall."
    end
  end

  def format_string(num_beers)
    if num_beers - 1 == 1
      "bottle"
    elsif num_beers - 1 == 0
      "No more bottles"
    else
      "bottles"
    end
  end

end

puts Bottles.new.verses(2, 0)
# puts Bottles.new.song
